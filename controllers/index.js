var express = require('express')
  , router = express.Router()

router.use('/mall', require('./mall'))

module.exports = router