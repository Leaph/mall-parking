var express = require('express')
  , router = express.Router()
  , Parking = require('../models/parking')

router.post('/:id/checkin', function(req, res) {
  var id = req.params.id-1
  var checkin = req.body.entryTime

  Parking.checkin(id, checkin, function (err, cb) {
    res.send('Checkin completed')
  })
})

router.get('/:id/checkprice', function(req, res) {
  Parking.getPrice(req.params.id-1, req.query.currentTime, function (err, cb) {
    if (cb == -1) res.send('You have not checked in!')
    else res.send('Current fee = '+cb)
  })
})

module.exports = router