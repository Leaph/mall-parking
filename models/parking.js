var start = [null, null, null]

// Siam Paragon (id=1)
// - Free for first 2 hours
// - 3rd - 6th hour, 30 baht/hour
// - 7th hour onwards, 60 baht/hour
// - 30 minutes or more = 1 hour
var paragon = []
for (var hours = 0; hours < 16; hours++) {
	if (hours < 2) paragon.push(0)
	else if (hours < 6) paragon.push(30*(hours-1))
	else paragon.push(paragon[5] + 60*(hours-5))
}
// Central World (id=2)
// - Free for first 30 minutes
// - 1st - 5th hour, 20 baht/hour
// - 6th hour onwards, 50 baht/hour
// - Fraction of hours = 1 hour
var ctw = []
for (var hours = 0; hours < 16; hours++) {
	if (hours < 5) ctw.push(20 * (hours+1))
	else ctw.push(ctw[4] + 50*(hours-4))
}
// Central Ladprao (id=3)
// - Free for first hour
// - First 3 hours, 20 baht
// - First 4 hours, 40 baht
// - 5th hour onwards, 50 baht/hour
// - 30 minutes or more = 1 hour
var ladprao = []
for (var hours = 0; hours < 16; hours++) {
	if (hours < 1) ladprao.push(0)
	else if (hours < 3) ladprao.push(20)
	else if (hours < 4) ladprao.push(40) 
	else ladprao.push(40 + 50*(hours-3))
}
var minutesToMili = 60*1000
var parking = [{"overflow":30*minutesToMili,"prices":paragon},
			   			 {"overflow":-1,"prices":ctw},
			   			 {"overflow":30*minutesToMili,"prices":ladprao}]

breakdown = function(difference) {
	var hours = Math.floor((difference / (60*minutesToMili)) % 24)
	var overflow = difference - (hours * 60*minutesToMili)
	return {"hours":hours, "overflow":overflow}
}

parseISOString = function(s) {
  var b = s.split(/\D+/);
  return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}

exports.checkin = function(id, date, cb) {
	start[id] = parseISOString(date)
  cb()
}

exports.getPrice = function(id, currentTime, cb) {
	var priceTable = parking[id]
	var parked = start[id]

	if (parked == null) return cb(null, -1)
	else {
		var time = breakdown(Math.abs(parseISOString(currentTime) - parked))
		var hours = time.hours
		var overflow = time.overflow
		if ((priceTable.overflow == -1 && overflow > 0)||(priceTable.overflow != -1 && overflow >= priceTable.overflow)) hours++

		var actualHour = hours-1
		if (actualHour < 0) return cb(null, 0)
		else if (actualHour > 15) return cb(null, "limit exceeded")
		return cb(null, priceTable.prices[actualHour])
	}
}